<?php

namespace App\Repository;

use App\Entity\Users;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Users|null find($id, $lockMode = null, $lockVersion = null)
 * @method Users|null findOneBy(array $criteria, array $orderBy = null)
 * @method Users[]    findAll()
 * @method Users[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UsersRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Users::class);
    }

//    /**
//     * @return Users[] Returns an array of Users objects
//     */
    /**
     * @param $login
     * @return mixed
     * This method select users with the same login than the $login send in the function
     */
    public function getUsersPerLogin($login)
    {
        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery(
            'SELECT u.id
        FROM  App\Entity\Users u
        WHERE u.login = :login'
        )->setParameter('login', $login);

        // returns an array of Product objects
        return $query->execute();
    }


    public function getUserWithoutStage(){
        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery(
            "
            SELECT users FROM App\Entity\Users users where users.id not in (select identity(stage.eleve) from App\Entity\Stage stage) and users.roles LIKE '%{%ROLE_STUDENT%}'
            "
        );

        return $query->execute();
    }

    public function getUsersReferred($id){

        # entity manager
        $entManager = $this->getEntityManager();

        #query
        $query = $entManager->createQuery("
        select users from App\Entity\Users users, App\Entity\Stage stage where stage.eleve = users.id and stage.Professeur = ".$id."

        ");

        return $query->execute();
}


    /*
    public function findOneBySomeField($value): ?Users
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
