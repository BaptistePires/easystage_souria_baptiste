<?php

namespace App\Controller;

use App\Entity\Stage;
use App\Entity\Users;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Tests\Fixtures\Type;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UsersController extends AbstractController
{
    /**
     * @Route("/profile", name="show_profile")
     */
    public function showProfile()
    {
        if ($this->getUser() == null) {
            return $this->redirectToRoute("index");
        }

        # set up the data to send to the view
        $data = array('login' => $this->getUser()->getLogin(),
            'name' => $this->getUser()->getUsername(),
            'lastname' => $this->getUser()->getName(),
            'password' => $this->getUser()->getPlainPassword()
        );

        return $this->render('users/index.html.twig', array('data' => $data));
    }

    /**
     * @Route("/profile/update_password", name="update_password")
     */
    public function updatePassword(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        if ($this->getUser() == null) {
            return $this->redirectToRoute("index");
        }

        #build the form to update the password
        $formb = $this->createFormBuilder()
            ->add('old_password', PasswordType::class)
            ->add('new_password', PasswordType::class)
            ->add('sec_new_password', PasswordType::class)
            ->add('create', SubmitType::class, array("label" => "Update Password"));

        $form = $formb->getForm();

        $form->handleRequest($request);

        # If the form is submitted
        if ($form->isSubmitted() && $form->isValid()) {
            $product_data = $form->getData();

            # Get form's data
            $oldPassword = $product_data["old_password"];
            $newPassword = $product_data["new_password"];
            $secNewPassword = $product_data["sec_new_password"];

            # Compare current password and entered password
            $oldPasswordGood = $passwordEncoder->isPasswordValid($this->getUser(), $oldPassword);

            # Compare new password and its confirmation
            if ($newPassword == $secNewPassword) {
                $newPasswordGood = True;
            } else {
                $newPasswordGood = False;
            }

            # If data that the user entered are true
            if ($newPasswordGood && $oldPasswordGood) {

                # encode the new password
                $encodedNewPassword = $passwordEncoder->encodePassword($this->getUser(), $newPassword);

                # get the manager
                $entityManager = $this->getDoctrine()->getManager();

                # set the user new password
                $this->getUser()->setPassword($encodedNewPassword);

                # update database
                $entityManager->flush();

                # adding a flash message to notify to the user that his password was updated successfully
                $this->addFlash("success", "Password changed correctly !");

                return $this->redirectToRoute("show_profile");
            } else {
                $this->addFlash("warning", "Wrong passwords");
                return $this->redirectToRoute("update_password");

            }


            // tell Doctrine you want to (eventually) save the Product (no queries yet)
//            $entityManager->persist($product_data);
            // actually executes the queries (i.e. the INSERT query)
//            $entityManager->flush();
//            return $this->redirectToRoute('show_profile');
        } else {
            return $this->render('users/update_password.html.twig', array('form' => $form->createView()));

        }
    }

    /**
     * @Route("/prof/usersWithoutStage", name="usersWithoutStage")
     */
    public function showUserWithoutStage()
    {
        $repository = $this->getDoctrine()->getRepository(Users::class);

        $users = $repository->getUserWithoutStage();

        return $this->render("users/showUsersWithoutStage.html.twig", array('users' => $users));


    }

    /**
     * @Route("/prof/referredStudent", name="showReferredStudent")
     */
    public function showReferredStudent()
    {
        # get repo
        $repo = $this->getDoctrine()->getRepository(Users::class);

        # users referred
        $users = $repo->getUsersReferred($this->getUser()->getId());

        return $this->render('users/showReferredStudents.html.twig', array("users" => $users));

    }

    /**
     * @Route("prof/referredStudent/delete/{id}", name="deleteOneReferredStudent")
     */
    public function cancelReferredStudent($id){

        # get stage repo
        $stageRepo = $this->getDoctrine()->getRepository(Stage::class);
        $stageEntityManager = $this->getDoctrine()->getManager();

        # Get the student stage
        $studentStage = $stageRepo->findOneBy(array('Eleve' => $id));

        # Check if the professor is the really referred to the stage
        if($studentStage->getReferent() != $this->getUser()){
            $this->addFlash('warning', "You are not the referred teacher of this student you can't cancel that.");
            return $this->redirectToRoute('showReferredStudent');
        }else{
            $studentStage->setReferent(null);
        }

        #update database
        $stageEntityManager->persist($studentStage);
        $stageEntityManager->flush();

        $this->addFlash('success', "Successfully cancelled !");
        return $this->redirectToRoute("showReferredStudent");
    }

}














