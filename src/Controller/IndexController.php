<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    /**
     * @Route("/index", name="index")
     */
    public function index(){
        date_default_timezone_set('Europe/Paris');
        $date = date('Y/m/d');
        return $this->render('index/index.html.twig',['date' => $date]);
    }







}
