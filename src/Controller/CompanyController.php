<?php

namespace App\Controller;

use App\Entity\Company;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Driver\PDOException;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class CompanyController extends AbstractController
{
    /**
     * @Route("/company/show", name="show_companies")
     */
    public function showAll()
    {

        # Get the repo
        $repo = $this->getDoctrine()->getRepository(Company::class);

        # Get All companies
        $companies = $repo->findAll();

        return $this->render('company/index.html.twig', [
            'companies' => $companies,
        ]);
    }

    /**
     * @Route("/company/add", name="add_company")
     */
    public function addCompany(Request $request)
    {

        # create a empty company
        $company = new Company();

        # Build the from
        $formBuild = $this->createFormBuilder($company)
            ->add('name', TextType::class)
            ->add('city', TextType::class)
            ->add('postal_code', TextType::class)
            ->add('address', TextType::class)
            ->add('mail', TextType::class)
            ->add('mobile_phone', TextType::class)
            ->add('activity', TextType::class);

        # Getting the form

        $formBuild->add('create', SubmitType::class, array("label" => "Add company"));

        $form = $formBuild->getForm();

        $form->handleRequest($request);

        # If the form is submitted
        if ($form->isSubmitted() && $form->isValid()) {
            $product_data = $form->getData();

            $entityManager = $this->getDoctrine()->getManager();

            // tell Doctrine you want to (eventually) save the Product (no queries yet)
            $entityManager->persist($product_data);
            // actually executes the queries (i.e. the INSERT query)
            $entityManager->flush();

            return $this->redirectToRoute('show_companies');
        }else{
            return $this->render('company/addCompany.html.twig',
                array( 'form' => $form->createView(), 'tab' => array('name','city','postal_code','address','mail','mobile_phone','activity')));
        }
    }



    /**
     * @Route("/admin/company/edit/{id}", name="company_edit")
     */
    public function edit($id, Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $company = $entityManager->getRepository(Company::class)->find($id);
        $form = $this->createFormBuilder($company)
            ->add('name', TextType::class)
            ->add('city', TextType::class)
            ->add('postal_code', TextType::class)
            ->add('address', TextType::class)
            ->add('mail', TextType::class)
            ->add('mobile_phone', TextType::class)
            ->add('activity', TextType::class)
            ->add('create', SubmitType::class, array("label" => "Edit"))
            ->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $company = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($company);
            $entityManager->flush();
            return $this->redirectToRoute('show_companies');
        }
        return $this->render('company/edit.html.twig', array(
            'form' => $form->createView()));
    }

    /**
     * @Route("/admin/product/delete/{id}", name="company_delete")
     */
    public function delete($id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $company = $entityManager->getRepository(Company::class)->find($id);

        try {
            $entityManager->remove($company);
            $entityManager->flush();
        }catch (ForeignKeyConstraintViolationException $e){
            $this->addFlash("danger", "Erreur ! Vous ne pouvez pas supprimer cette entreprise car elle est déjà liée à un stage.".$e->getMessage());
            return $this->redirectToRoute("show_companies");
        }


        return $this->redirectToRoute('show_companies');
    }







}
